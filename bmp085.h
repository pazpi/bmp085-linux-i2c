#ifndef __BMP085_H__
#define __BMP085_H__ 

// DEFINE
// Blinks on RPi Plug P1 pin 11 (which is GPIO pin 17)
#define PIN_OUT RPI_GPIO_P1_11
#define PIN_IN  RPI_GPIO_P1_15
#define PIN_IN_2  RPI_GPIO_P1_12
// BMP085 PARAMS
#define BMP085_I2CADDR 		 0x77
#define BMP085_CAL_AC1           0xAA  // Calibration data (16 bits)
#define BMP085_CAL_AC2           0xAC  // Calibration data (16 bits)
#define BMP085_CAL_AC3           0xAE  // Calibration data (16 bits)    
#define BMP085_CAL_AC4           0xB0  // Calibration data (16 bits)
#define BMP085_CAL_AC5           0xB2  // Calibration data (16 bits)
#define BMP085_CAL_AC6           0xB4  // Calibration data (16 bits)
#define BMP085_CAL_B1            0xB6  // Calibration data (16 bits)
#define BMP085_CAL_B2            0xB8  // Calibration data (16 bits)
#define BMP085_CAL_MB            0xBA  // Calibration data (16 bits)
#define BMP085_CAL_MC            0xBC  // Calibration data (16 bits)
#define BMP085_CAL_MD            0xBE  // Calibration data (16 bits)

#define BMP085_CONTROL           0xF4 
#define BMP085_TEMPDATA          0xF6
#define BMP085_PRESSUREDATA      0xF6
#define BMP085_READTEMPCMD       0x2E
#define BMP085_READPRESSURECMD   0x34

// Function header declaration
int initSensor();
uint16_t readI2cWord(uint8_t); // First byte, example 0xF6 -> 0xF6F7 instead of 0xF7F6
uint16_t readRawTemp();
uint32_t readRawPressure(uint8_t);
int16_t readTemp();
uint32_t readPressure(uint8_t);
float altitude(uint32_t);

void tempLog();

// Exit and print error code
static void exit_on_error (const char *s){
    perror(s);
    abort();
} 

#endif
