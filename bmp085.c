// bmp085.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/i2c-dev.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
// bmp085 define
#include "bmp085.h"

#define MAX_CHAR 1024
#define MAX_TEMP_REG 100

// #define DEBUG
#define TEMP
// #define PRESSURE
// #define DEBUG_i2c

// Global variables
int16_t AC1, AC2, AC3, B1, B2, MB, MC, MD;
uint16_t AC4, AC5, AC6;

static const char *device = "/dev/i2c-1";	// I2C bus

int main(int argc, char **argv)
{  
#ifdef PRESSURE
    if(argc > 3 || argc == 1){
        printf("Usage: %s [Precision pressure read]", argv[0]);
        exit(0);
    }
    int oss = (int)(argv[1][0] - '0');
#endif
    initSensor();
#ifdef TEMP
    tempLog();
#endif
#ifdef DEBUG
    printf("argv: %d\n", oss);
#endif
#ifdef PRESSURE
    uint32_t pressure = readPressure(oss);
    printf("Pressure: %d [hPa]\n", pressure/100);
    printf("Altitude: %4.2f [m]\n", altitude(101325));
#endif
    return 0;
}

void tempLog(){
    time_t t = time(NULL);
    struct tm tm;
    tm = *localtime(&t);

    int min_t, max_t, sum_t;
    min_t = 0xffff;
    max_t = sum_t = 0;
    float avrg_t = 0.0;
    while(1){
        int min_mean_t, max_mean_t;
        min_mean_t = 0xffff, max_mean_t = 0;
            int i;
            for(i=0; i<MAX_TEMP_REG;){
            FILE *fp;
            fp = fopen("temp.log", "a");

            int year, month, day, hour, min, sec;
            char cur_tempo[MAX_CHAR];

            int temp = readTemp();

            year = tm.tm_year + 1900; month = tm.tm_mon + 1; day = tm.tm_mday; hour = tm.tm_hour + 1; min = tm.tm_min; sec = tm.tm_sec;
            if(tm.tm_isdst == 0){
                hour += 1;
            }
            sprintf(cur_tempo, "%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d", year, month, day, hour, min, sec);    
            fprintf(fp, "%s\t%4.2f\n", cur_tempo, (float)temp/10);
            
            if(temp<min_mean_t) min_mean_t = temp;
            if(temp>max_mean_t) max_mean_t = temp;
            if(temp<min_t) min_t = temp;
            if(temp>max_t) max_t = temp;

            avrg_t = (min_mean_t + max_mean_t) / 2;

            printf("i: %2d Temp: %4.1f[°C] min/avg/max: %4.1f/%4.1f/%4.1f ", i, (float)temp/10, (float)min_t/10, (float)avrg_t/10, (float)max_t/10);
            printf("Tempo %s\n", cur_tempo);
            
            fclose(fp);
            delay(1*1000);
        }
    }

}

int initSensor(){
    AC1 = readI2cWord(BMP085_CAL_AC1);
    AC2 = readI2cWord(BMP085_CAL_AC2);
    AC3 = readI2cWord(BMP085_CAL_AC3);
    AC4 = readI2cWord(BMP085_CAL_AC4);
    AC5 = readI2cWord(BMP085_CAL_AC5);
    AC6 = readI2cWord(BMP085_CAL_AC6);
    B1 = readI2cWord(BMP085_CAL_B1);
    B2 = readI2cWord(BMP085_CAL_B2);
    MB = readI2cWord(BMP085_CAL_MB);
    MC = readI2cWord(BMP085_CAL_MC);
    MD = readI2cWord(BMP085_CAL_MD);
#ifdef DEBUG
    printf("Data read AC1: %d - 0x%2.2X\n", AC1, AC1);      
    printf("Data read AC2: %d - 0x%2.2X\n", AC2, AC2);      
    printf("Data read AC3: %d - 0x%2.2X\n", AC3, AC3);      
    printf("Data read AC4: %d - 0x%2.2X\n", AC4, AC4);      
    printf("Data read AC5: %d - 0x%2.2X\n", AC5, AC5);      
    printf("Data read AC6: %d - 0x%2.2X\n", AC6, AC6);      
    printf("Data read B1: %d - 0x%2.2X\n", B1, B1);      
    printf("Data read B2: %d - 0x%2.2X\n", B2, B2);      
    printf("Data read MB: %d - 0x%2.2X\n", MB, MB);      
    printf("Data read MC: %d - 0x%2.2X\n", MC, MC);      
    printf("Data read MD: %d - 0x%2.2X\n", MD, MD);      
#endif
    return 0;
}

uint16_t readRawTemp(){
    int fd;
    // Open I2C device
    if ((fd = open(device, O_RDWR)) < 0) exit_on_error ("Can't open I2C device");
    // Set I2C slave address
    if (ioctl(fd, I2C_SLAVE, BMP085_I2CADDR) < 0) exit_on_error ("Can't talk to slave");
    // Configure read temperature      
    if ( i2c_smbus_write_byte_data(fd , BMP085_CONTROL, BMP085_READTEMPCMD ) < 0) exit_on_error ("Failed to write to the i2c bus [1]");
    close(fd);
    delay(5);
    uint16_t rawTemp = readI2cWord(BMP085_TEMPDATA);
    return rawTemp;
}

int16_t readTemp(){
    uint16_t UT = readRawTemp();
#ifdef DEBUG
    printf("UT: %d - 0x%2.2X\n", UT, UT);
#endif
    int32_t X1 = ((UT - AC6) * AC5) >> 15;
    int32_t X2 = (MC << 11) / (X1 + MD);
    int32_t B5 = X1 + X2;
    int32_t T = (B5 + 8) >> 4;
#ifdef DEBUG
    printf("T: %d\n", T);
#endif
    return T;
}

// oss = number of samples, 0 ULTRA LOW POWER, 3 ULTRA HIGH RESOLUTION
uint32_t readRawPressure(uint8_t oss){
    int fd;
    // Open I2C device
    if ((fd = open(device, O_RDWR)) < 0) exit_on_error ("Can't open I2C device");
    // Set I2C slave address
    if (ioctl(fd, I2C_SLAVE, BMP085_I2CADDR) < 0) exit_on_error ("Can't talk to slave");
    // Configure read temperature      
    if ( i2c_smbus_write_byte_data(fd , BMP085_CONTROL, BMP085_READPRESSURECMD + (oss << 6 )) < 0) exit_on_error ("Failed to write to the i2c bus [1]");
    close(fd);
    switch(oss){
        case 0: delay(5); break;
        case 1: delay(8); break;
        case 2: delay(14); break;
        case 3: delay(26); break;
    }
    uint32_t rawPressure = readI2cWord(BMP085_PRESSUREDATA);
    rawPressure <<=8;
    rawPressure |= (readI2cWord((BMP085_PRESSUREDATA+2)>>8));
    rawPressure >>= (8 - oss);
    return rawPressure;
}

uint32_t readPressure(uint8_t oss){
    int32_t UP = readRawPressure(oss);
    int32_t UT = readRawTemp();
    uint32_t B4, B7;
    int32_t X1, X2, X3, B3, B5, B6, p;
#ifdef DEBUG
    printf("UP: %d - %2.2X\n", UP, UP);
#endif
    X1 = ((UT - AC6) * AC5) >> 15;
    X2 = (MC << 11) / (X1 + MD);
    B5 = X1 + X2;
    B6 = B5 - 4000;
    X1 = ((int32_t)B2 * ((B6 * B6) >> 12)) >> 11;
    X2 = ((int32_t)AC2 * B6)>>11;
    X3 = X1 + X2;
    B3 = ((((int32_t)AC1 * 4 + X3)<<oss) + 2) / 4;

    X1 = ((int32_t)AC3 * B6)>>13;
    X2 = ((int32_t)B1 * (B6 * B6>>12))>>16;
    X3 = ((X1 + X2) + 2)>>2;
    B4 = (uint32_t)AC4 * (uint32_t)(X3 + 32768)>>15;
    B7 = ((uint32_t)UP - B3) * (uint32_t)(50000UL>>oss);
    if(B7 < 0x80000000){  
        p = (B7 * 2) / B4;
    } else{               
        p = (B7 / B4) * 2;
    }
    X1 = (p>>8) * (p>>8);
    X1 = (X1 * 3038)>>16;
    X2 = (-7357 * p)>>16;
    p = p + ((X1 + X2 + 3791)>>4);		
    return p;
}

uint16_t readI2cWord(uint8_t addr){
    int fd;
    uint16_t msb;
    uint16_t lsb;
    // Open I2C device
    if ((fd = open(device, O_RDWR)) < 0) exit_on_error ("Can't open I2C device");
    // Set I2C slave address
    if (ioctl(fd, I2C_SLAVE, BMP085_I2CADDR) < 0) exit_on_error ("Can't talk to slave");
    // Configure read temperature      
    if ( i2c_smbus_write_byte_data(fd , BMP085_CONTROL, BMP085_READTEMPCMD ) < 0) exit_on_error ("Failed to write to the i2c bus [1]");
    delay(5);
    if ( (msb = i2c_smbus_read_byte_data (fd , addr )) < 0) exit_on_error ("Failed to read from the i2c bus [1]");
    if ( (lsb = i2c_smbus_read_byte_data (fd , (addr+1) )) < 0) exit_on_error ("Failed to read from the i2c bus [1]");
#ifdef DEBUG_i2c
    printf("Data read MSB 0x%X: 0x%2.2X\n", addr, msb);      
    printf("Data read LSB 0x%X: 0x%2.2X\n", (addr + 1), lsb);      
#endif
    // I dati letti vanno messi nella forma 0xMSBLSB
    uint16_t word = (msb << 8) | lsb;
#ifdef DEBUG_i2c
    printf("Word: %d - 0x%2.2X\n", word, word);
#endif
    close(fd);
    return word;
}

float altitude(uint32_t sealevelPressure){
    float altitude;
    uint32_t pressure = readPressure(0);
    altitude = 44330 * (1.0 - pow((float)pressure / sealevelPressure, 0.190285));
    return altitude;
}

